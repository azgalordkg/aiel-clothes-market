from django.db.models import Q
from django.forms import HiddenInput
from django_filters import FilterSet, CharFilter, ModelMultipleChoiceFilter

from main.models import Category, Product
from main.widgets import CategorySelectWidget


class ProductSearchFilter(FilterSet):

    class Meta:
        model = Product
        exclude = ('main_image', )

    title = CharFilter(method='filter_title')
    category = ModelMultipleChoiceFilter(queryset=Category.objects.all(), widget=CategorySelectWidget)
    order_by_price = CharFilter(method='filter_order_by_price')
    order_by_newest = CharFilter(method='filter_order_by_newest')

    order_by = CharFilter(method='filter_order_by', widget=HiddenInput)

    def filter_title(self, queryset, name, value):
        return queryset.filter(Q(title__iregex=r'(%s)' % value) | Q(code__iregex=r'(%s)' % value))

    def filter_order_by(self, queryset, name, value):
        if value == 'price':
            return queryset.order_by('price')
        elif value == 'newest':
            return queryset.order_by('-created_at')

        return queryset