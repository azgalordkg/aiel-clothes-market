import json

from django import template

from main.models import Product

register = template.Library()


@register.filter(name='str')
def to_str(v):
    return str(v)


@register.simple_tag
def cart_items_count(request):
    return len(json.loads(request.COOKIES['cart'])) if 'cart' in request.COOKIES else 0


@register.simple_tag
def get_cart_items(request, cart=None):
    items = []
    _cart_items = []

    if cart is not None:
        _cart_items = cart
    elif 'cart' in request.COOKIES:
        _cart_items = json.loads(request.COOKIES['cart'])

    for i in _cart_items:
        try:
            product = Product.objects.get(pk=i['product_id'])

            items.append({
                'product': product,
                'quantity': i['quantity'],
            })
        except Product.DoesNotExist:
            pass

    return items


@register.simple_tag
def get_total_sum(cart_items):
    total = 0

    for i in cart_items:
        total += i['product'].price_linear * i['quantity']

    return total


@register.filter
def swap_categories_images(categories):
    cats = list(categories)

    if len(cats) == 1:
        cats.append(
            {
                'image': cats[0].image,
                'fake': True
            }
        )
    elif len(cats) >= 2:
        temp = cats[0].image
        cats[0].image = cats[1].image
        cats[1].image = temp

    return cats