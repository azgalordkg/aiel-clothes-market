from django.forms import SelectMultiple
from django.template import loader
from django.utils.safestring import mark_safe


class CategorySelectWidget(SelectMultiple):
    template_name = 'widgets/category_select.html'

    def render(self, name, value, attrs=None):
        context = self.get_context(name, value, attrs)
        template = loader.get_template(self.template_name).render(context)
        return mark_safe(template)

    def get_context(self, name, value, attrs):
        context = super(CategorySelectWidget, self).get_context(name, value, attrs)
        context['choices'] = self.choices.queryset

        return context