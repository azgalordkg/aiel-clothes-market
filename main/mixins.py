import json

from django.http import HttpResponse


class CartMixin(object):

    def add_to_cart(self, response, product_id, quantity, replace_quantity):
        cart = self._get_cart()
        item_found = False

        for index, i in enumerate(cart):
            if i['product_id'] == product_id:
                if replace_quantity:
                    i['quantity'] = quantity
                else:
                    i['quantity'] = int(i['quantity']) + 1
                cart[index] = i
                item_found = True

        if not item_found:
            cart.append({
                'product_id': product_id,
                'quantity': quantity
            })

        response.set_cookie('cart', json.dumps(cart))

        return cart

    def remove_from_cart(self, response, product_id):
        cart = self._get_cart()

        for index, i in enumerate(cart):
            if i['product_id'] == product_id:
                del cart[index]

        response.set_cookie('cart', json.dumps(cart))

        return cart

    def clear_cart(self, response):
        response.set_cookie('cart', '[]')

    def _get_cart(self):
        return json.loads(self.request.COOKIES['cart']) if 'cart' in self.request.COOKIES else []

    def create_response(self):
        http_resp = HttpResponse(content_type='application/json')

        return http_resp