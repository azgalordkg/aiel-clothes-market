from django.contrib import admin

# Register your models here.
from django.contrib.admin import TabularInline
from ordered_model.admin import OrderedModelAdmin

from main.forms import CategoryForm, ProductAdminForm
from .models import *


class SubcategoryInline(TabularInline):
    model = Category
    verbose_name_plural = 'Подкатегории'


class ProductImageInline(TabularInline):
    model = ProductImage
    verbose_name_plural = 'Изображения товара'


class OrderItemInline(TabularInline):
    model = OrderItem
    verbose_name_plural = 'Элементы заказа'
    readonly_fields = ['code', 'title', 'size', 'length', 'color', 'price', 'quantity', 'total_sum', ]
    exclude = ['product']

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def price(self, obj):
        return obj.product.price_linear

    def total_sum(self, obj):
        return self.price(obj) * obj.quantity

    def title(self, obj):
        return obj.product.title

    def code(self, obj):
        return obj.product.code

    def size(self, obj):
        return obj.product.size

    def length(self, obj):
        length = obj.product.length
        return length if length else '-'

    def color(self, obj):
        return obj.product.color

    price.short_description = 'Цена за линейку'
    total_sum.short_description = 'Итого'
    title.short_description = 'Название'
    code.short_description = 'Артикул'
    size.short_description = 'Размер'
    length.short_description = 'Длина'
    color.short_description = 'Цвет'


class CategoryAdmin(OrderedModelAdmin):
    list_display = ['title', 'parent', 'move_up_down_links']
    inlines = (SubcategoryInline, )
    ordering = ('order',)

    form = CategoryForm


admin.site.register(Category, CategoryAdmin)


class ProductAdmin(OrderedModelAdmin):
    list_display = ['title', 'category', 'move_up_down_links']
    inlines = (ProductImageInline, )
    ordering = ('order', )
    form = ProductAdminForm


admin.site.register(Product, ProductAdmin)


class OrderAdmin(admin.ModelAdmin):
    list_display = ['name', 'email', 'phone', 'processed', 'created_at', ]
    inlines = (OrderItemInline, )

    def has_add_permission(self, request):
        return False


# admin.site.register(Order, OrderAdmin)


class AboutCounterInline(TabularInline):
    model = AboutCounter


class AboutAdmin(admin.ModelAdmin):
    list_display = ['id']
    inlines = (AboutCounterInline, )


admin.site.register(About, AboutAdmin)


class ConditionAdmin(OrderedModelAdmin):
    list_display = ['title', 'short_description', 'move_up_down_links']


admin.site.register(Condition, ConditionAdmin)


# class PriceListRequestAdmin(admin.ModelAdmin):
#     list_display = ['email', 'processed', 'created_at']
#
#     def has_add_permission(self, request):
#         return False


# admin.site.register(PriceListRequest, PriceListRequestAdmin)


class RequisitesAdmin(admin.ModelAdmin):
    list_display = ['id']


admin.site.register(Requisites, RequisitesAdmin)


class ContactsAdmin(admin.ModelAdmin):
    list_display = ['id']


admin.site.register(Contacts, ContactsAdmin)


class SocialAdmin(admin.ModelAdmin):
    list_display = ['fb', 'instagram', 'vk', 'disqus', 'updated_at', ]


admin.site.register(Social, SocialAdmin)


class CallbackAdmin(admin.ModelAdmin):
    list_display = ['name', 'phone', 'email', 'processed', 'created_at']

    def has_add_permission(self, request):
        return False


admin.site.register(Callback, CallbackAdmin)


class ConditionsPageBuyStepInline(TabularInline):
    model = ConditionsPageBuyStep


class ConditionsPageAdmin(admin.ModelAdmin):
    list_display = ['id']
    inlines = (ConditionsPageBuyStepInline, )


admin.site.register(ConditionsPage, ConditionsPageAdmin)


class DeliveryAdmin(admin.ModelAdmin):
    list_display = ['id']


admin.site.register(Delivery, DeliveryAdmin)


class RetailRequestAdmin(admin.ModelAdmin):
    list_display = ['id', 'document', 'created_at']


# admin.site.register(RetailRequest, RetailRequestAdmin)


class RetailShopRequestAdmin(admin.ModelAdmin):
    list_display = ['title', 'processed', 'created_at']

    def has_add_permission(self, request):
        return False


admin.site.register(RetailShopRequest, RetailShopRequestAdmin)


class RetailShopCategoryAdmin(admin.ModelAdmin):
    list_display = ['title', ]


admin.site.register(RetailShopCategory, RetailShopCategoryAdmin)

