import json

from django.db.models import Q
from django.http import Http404
from django.shortcuts import render, render_to_response

# Create your views here.
from django.template import RequestContext
from django.views.generic import TemplateView, DetailView
from rest_framework.generics import CreateAPIView
from rest_framework.views import APIView

from main.filter import ProductSearchFilter
from main.forms import OrderForm, PriceListRequestForm, RetailRequestForm, RetailShopRequestForm
from main.mixins import CartMixin
from main.models import Social, Category, Product, OrderItem, About, Condition, Contacts, Requisites, ConditionsPage, \
    Delivery
from main.serializers import CartItemSerializer, OrderSerializer, PriceListRequestSerializer, CallbackSerializer, \
    RetailRequestSerializer, RetailShopRequestSerializer
from main.templatetags.aiel import get_total_sum, get_cart_items


class IndexView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, *args, **kwargs):
        context = super(IndexView, self).get_context_data(*args, **kwargs)
        context['categories'] = Category.objects.filter(parent__isnull=True).order_by('order')[:2]
        context['about'] = About.objects.first()

        return context


class AboutUsView(TemplateView):
    template_name = 'about.html'

    def get_context_data(self, *args, **kwargs):
        context = super(AboutUsView, self).get_context_data(*args, **kwargs)
        context['about'] = About.objects.first()

        return context


class ConditionsView(TemplateView):
    template_name = 'conditions.html'

    def get_context_data(self, *args, **kwargs):
        context = super(ConditionsView, self).get_context_data(*args, **kwargs)
        context['conditions'] = Condition.objects.all()
        context['condition_page'] = ConditionsPage.objects.first()

        return context


class PaymentView(TemplateView):
    template_name = 'payments.html'

    def get_context_data(self, *args, **kwargs):
        context = super(PaymentView, self).get_context_data(*args, **kwargs)
        context['req'] = Requisites.objects.first()
        context['plr_form'] = PriceListRequestForm()

        return context


class DeliveryView(TemplateView):
    template_name = 'delivery.html'

    def get_context_data(self, **kwargs):
        context = super(DeliveryView, self).get_context_data(**kwargs)
        context['delivery'] = Delivery.objects.first()

        return context


class CartView(TemplateView):
    template_name = 'cart.html'

    def get_context_data(self, *args, **kwargs):
        context = super(CartView, self).get_context_data(*args, **kwargs)
        context['order_form'] = OrderForm()
        context['retail_request_form'] = RetailRequestForm()
        context['req'] = Requisites.objects.first()

        return context


class CatalogueView(TemplateView):
    template_name = 'catalog.html'

    def get_context_data(self, *args, **kwargs):
        context = super(CatalogueView, self).get_context_data(*args, **kwargs)

        qs = Product.objects.order_by('order')

        try:
            main_category = Category.objects.get(pk=self.kwargs['category_id'], parent__isnull=True)
            qs = qs.filter(category__parent=main_category)
        except Category.DoesNotExist:
            raise Http404()

        context['filter'] = ProductSearchFilter(data=self.request.GET, queryset=qs)

        form = context['filter'].form
        form.fields['category'].queryset = Category.objects.filter(Q(pk=main_category.pk) | Q(parent=main_category))

        context['form'] = form
        context['main_category'] = main_category
        subcategory_ids = list(filter(lambda v: v not in [None, ''], self.request.GET.getlist('category')))

        context['subcategories'] = Category.objects.filter(pk__in=subcategory_ids, parent__isnull=False)

        return context


class ProductDetailView(DetailView):
    template_name = 'product_detail.html'
    model = Product
    context_object_name = 'product'


class ConditionsDetailView(DetailView):
    model = Condition
    template_name = 'conditions_detail.html'


class ContactsView(TemplateView):
    template_name = 'contacts.html'

    def get_context_data(self, *args, **kwargs):
        context = super(ContactsView, self).get_context_data(*args, **kwargs)
        context['contacts'] = Contacts.objects.first()

        return context


####

class CustomCreateAPIView(CreateAPIView, CartMixin):

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = self.perform_create(serializer)

        return response

    def perform_create(self, serializer):
        response = self.create_response()
        serializer.save()

        response.content = json.dumps(self.get_json())

        return response

    def get_json(self):
        pass


class AddToCartView(APIView, CartMixin):

    def post(self, request, *args, **kwargs):
        serializer = CartItemSerializer(data=request.POST)
        serializer.is_valid(raise_exception=True)

        data = serializer.validated_data
        product_id = data['product_id']
        quantity = data['quantity']
        replace_quantity = data['replace_quantity'] == 1

        response = self.create_response()

        cart = self.add_to_cart(response, product_id, quantity, replace_quantity)

        response.content = json.dumps({
            'success': True,
            'message': 'Товар добавлен в корзину' if not replace_quantity else False,
            'items_count': len(cart),
            'total_sum': get_total_sum(get_cart_items(request, cart))
        })

        return response


class RemoveFromCartView(APIView, CartMixin):

    def post(self, request, *args, **kwargs):
        serializer = CartItemSerializer(data=request.POST)
        serializer.is_valid(raise_exception=True)

        data = serializer.validated_data
        product_id = data['product_id']

        response = self.create_response()
        cart = self.remove_from_cart(response, product_id)

        response.content = json.dumps({
            'success': True,
            'message': 'Товар удален из корзины',
            'items_count': len(cart),
            'total_sum': get_total_sum(get_cart_items(request, cart))
        })

        return response


class OrderCreateView(CustomCreateAPIView, CartMixin):
    serializer_class = OrderSerializer

    def perform_create(self, serializer):
        response = self.create_response()
        cart_items = get_cart_items(self.request)
        json_body = {
            'success': True,
            'message': 'Заявка успешно отправлена.',
            'items_count': 0,
            'total_sum': 0
        }

        if len(cart_items) == 0:
            json_body = {
                'success': False,
                'message': 'Корзина пуста. Добавьте товары.'
            }
        else:
            instance = serializer.save()

            for i in cart_items:
                OrderItem.objects.create(product=i['product'], quantity=i['quantity'], order=instance)

            self.clear_cart(response)

        response.content = json.dumps(json_body)

        return response


class PriceListRequestCreateView(CustomCreateAPIView):
    serializer_class = PriceListRequestSerializer

    def get_json(self):
        return {
            'success': True,
            'message': 'Ваша заявка принята. Мы отправим прайс-лист в близжайшее время.'
        }


class CallbackCreateView(CustomCreateAPIView):
    serializer_class = CallbackSerializer

    def get_json(self):
        return {
            'success': True,
            'message': 'Заявка успешно принята.'
        }


class RetailRequestCreateView(CustomCreateAPIView):
    serializer_class = RetailRequestSerializer

    def get_json(self):
        return {
            'success': True,
            'message': 'Заявка успешно принята.'
        }


class RetailShopRequestCreateView(CustomCreateAPIView):
    serializer_class = RetailShopRequestSerializer

    def get_json(self):
        return {
            'success': True,
            'message': 'Заявка успешно принята.'
        }


class RetailView(TemplateView):
    template_name = 'retail.html'


class PriceFormView(TemplateView):
    template_name = 'price-form.html'

    def get_context_data(self, **kwargs):
        context = super(PriceFormView, self).get_context_data(**kwargs)

        form = RetailShopRequestForm()
        form.fields['category'].queryset = Category.objects.filter(parent__isnull=True)

        context['form'] = form

        return context


def page_not_found(request, *args, **argv):
    return render(request, '404.html')
