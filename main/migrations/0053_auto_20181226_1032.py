# Generated by Django 2.0.5 on 2018-12-26 04:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0052_auto_20181221_1410'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='retailshoprequest',
            options={'verbose_name': 'Заявка для получения прайс-листа', 'verbose_name_plural': 'Заявки для получения прайс-листа'},
        ),
        migrations.AddField(
            model_name='retailshoprequest',
            name='processed',
            field=models.BooleanField(default=False, verbose_name='Обработан?'),
        ),
    ]
