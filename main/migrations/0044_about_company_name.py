# Generated by Django 2.0.5 on 2018-12-17 11:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0043_category_order'),
    ]

    operations = [
        migrations.AddField(
            model_name='about',
            name='company_name',
            field=models.CharField(default='', max_length=255, verbose_name='Название компании'),
        ),
    ]
