# Generated by Django 2.0.5 on 2018-11-19 10:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0022_auto_20181119_1601'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='condition',
            options={'ordering': ('order',), 'verbose_name': 'Условие', 'verbose_name_plural': 'Условия'},
        ),
        migrations.RemoveField(
            model_name='condition',
            name='created_at',
        ),
        migrations.RemoveField(
            model_name='condition',
            name='updated_at',
        ),
        migrations.AddField(
            model_name='condition',
            name='order',
            field=models.PositiveIntegerField(db_index=True, default=0, editable=False, verbose_name='order'),
            preserve_default=False,
        ),
    ]
