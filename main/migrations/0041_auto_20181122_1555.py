# Generated by Django 2.0.5 on 2018-11-22 09:55

import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0040_contacts_jivosite_code'),
    ]

    operations = [
        migrations.AlterField(
            model_name='conditionspage',
            name='warning_text',
            field=ckeditor.fields.RichTextField(verbose_name='Текст-предупреждение'),
        ),
    ]
