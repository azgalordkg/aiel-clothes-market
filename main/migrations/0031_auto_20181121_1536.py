# Generated by Django 2.0.5 on 2018-11-21 09:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0030_auto_20181121_1534'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contacts',
            name='phone',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Телефон'),
        ),
        migrations.AlterField(
            model_name='contacts',
            name='whatsapp',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Whatsapp'),
        ),
    ]
