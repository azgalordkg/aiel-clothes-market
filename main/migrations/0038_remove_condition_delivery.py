# Generated by Django 2.0.5 on 2018-11-22 08:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0037_auto_20181122_1402'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='condition',
            name='delivery',
        ),
    ]
