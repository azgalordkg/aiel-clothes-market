# Generated by Django 2.0.5 on 2018-12-18 04:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0045_auto_20181217_1737'),
    ]

    operations = [
        migrations.CreateModel(
            name='RetailRequest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True, verbose_name='Создано')),
                ('updated_at', models.DateTimeField(auto_now=True, null=True, verbose_name='Обновлено')),
                ('document', models.FileField(upload_to='', verbose_name='document')),
            ],
            options={
                'verbose_name_plural': 'Заявки оптовиков',
                'verbose_name': 'Заявка оптовиков',
            },
        ),
    ]
