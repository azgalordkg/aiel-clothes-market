# Generated by Django 2.1.2 on 2018-10-23 06:46

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0003_auto_20181023_0520'),
    ]

    operations = [
        migrations.RenameField(
            model_name='category',
            old_name='iamge',
            new_name='image',
        ),
    ]
