from django.utils.safestring import mark_safe

from main.models import Order, Callback, RetailShopRequest

try:
    from django.utils.deprecation import MiddlewareMixin
except ImportError:
    MiddlewareMixin = object


class AdminMiddleware(MiddlewareMixin):

    def process_request(self, request):
        if request.path_info.startswith('/admin/'):
            order_quantity = Order.objects.filter(processed=False).count()

            if order_quantity > 0:
                Order._meta.verbose_name_plural = mark_safe('Заказы (%s)' % order_quantity)
            else:
                Order._meta.verbose_name_plural = 'Заказы'

            plr_quantity = RetailShopRequest.objects.filter(processed=False).count()

            if plr_quantity > 0:
                RetailShopRequest._meta.verbose_name_plural = mark_safe('Заявки для получения прайс-листа (%s)' % plr_quantity)
            else:
                RetailShopRequest._meta.verbose_name_plural = 'Заявки для получения прайс-листа'

            callback_quantity = Callback.objects.filter(processed=False).count()

            if callback_quantity > 0:
                Callback._meta.verbose_name_plural = mark_safe('Обратный звонок (%s)' % callback_quantity)
            else:
                Callback._meta.verbose_name_plural = 'Обратный звонок'