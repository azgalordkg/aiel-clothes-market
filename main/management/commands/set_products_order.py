from django.core.management import BaseCommand

from main.models import Condition, Product


class Command(BaseCommand):

    def handle(self, *args, **options):
        for index, obj in enumerate(Product.objects.all()):
            obj.order = index
            obj.save()
