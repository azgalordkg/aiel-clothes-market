from django.core.management import BaseCommand

from main.models import Condition, Product, Category


class Command(BaseCommand):

    def handle(self, *args, **options):
        for index, obj in enumerate(Category.objects.all()):
            obj.order = index
            obj.save()
