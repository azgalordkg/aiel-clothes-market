from django.core.management import BaseCommand

from main.models import Condition


class Command(BaseCommand):

    def handle(self, *args, **options):
        for index, obj in enumerate(Condition.objects.all()):
            obj.order = index
            obj.save()
