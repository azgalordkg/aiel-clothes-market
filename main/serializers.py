from rest_framework.fields import IntegerField, BooleanField
from rest_framework.serializers import Serializer, ModelSerializer

from main.models import Order, PriceListRequest, Callback, RetailRequest, RetailShopRequest


class CartItemSerializer(Serializer):

    product_id = IntegerField(required=True)
    quantity = IntegerField(required=False)
    replace_quantity = BooleanField(required=False)


class OrderSerializer(ModelSerializer):

    class Meta:
        model = Order
        exclude = ()


class PriceListRequestSerializer(ModelSerializer):

    class Meta:
        model = PriceListRequest
        exclude = ()


class CallbackSerializer(ModelSerializer):

    class Meta:
        model = Callback
        exclude = ()

    def validate(self, attrs):
        attrs['phone'] = attrs['phone'].replace(' ', '')

        return attrs


class RetailRequestSerializer(ModelSerializer):

    class Meta:
        model = RetailRequest
        exclude = ()


class RetailShopRequestSerializer(ModelSerializer):

    class Meta:
        model = RetailShopRequest
        exclude = ()