from main.forms import CallbackForm
from main.models import Social, Contacts


def general(request):
    return {
        'social': Social.objects.first(),
        'callback_form': CallbackForm(),
        'contacts': Contacts.objects.first()
    }