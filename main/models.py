from ckeditor.fields import RichTextField
from django.contrib.auth.models import AbstractUser
from django.db import models


# Create your models here.
from django.db.models import CharField, TextField
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.safestring import mark_safe
from ordered_model.models import OrderedModel

ICON_CHOICES = (
    ('1', 'Корзина'),
    ('2', 'Автомобиль'),
    ('3', 'Рубль'),
    ('4', 'Документы'),
    ('5', 'Портфель'),
    ('6', 'Скидка'),
)


class User(AbstractUser):
    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'


from akoikelov.djazz.models import AbstractModel


class Category(AbstractModel, OrderedModel):
    class Meta:
        verbose_name_plural = 'Категории'
        verbose_name = 'Категория'

    title = models.CharField(verbose_name='Название', max_length=255, unique=False, null=False)
    image = models.ImageField(verbose_name='Изображение', null=True, blank=True)
    parent = models.ForeignKey(verbose_name='Родительская категория', to='Category', on_delete=models.CASCADE, null=True,
                               blank=True, related_name='subcategories')

    def __unicode__(self):
        return self.title


class Product(AbstractModel, OrderedModel):
    class Meta:
        verbose_name_plural = 'Товары'
        verbose_name = 'Товар'

    title = models.CharField(verbose_name='Название', max_length=255, unique=False, null=False)
    category = models.ForeignKey(verbose_name='Категория', to=Category, on_delete=models.CASCADE, null=False)
    code = models.CharField(verbose_name='Код', max_length=255, unique=False, null=False)
    size = models.CharField(verbose_name='Размеры в линейке', max_length=255, unique=False, null=False)
    length = models.CharField(verbose_name='Длина', max_length=255, unique=False, null=True, default='-', blank=True)
    color = models.CharField(verbose_name='Цвет', max_length=255, unique=False, null=False)
    price = models.FloatField(verbose_name='Цена', null=False, default=0, blank=True)
    price_linear = models.FloatField(verbose_name='Цена за 1 линейку', null=False, default=1, blank=True)
    quantity_in_linear = models.IntegerField(verbose_name='Количество штук в линейке', null=False, default=1)
    main_image = models.ImageField(verbose_name='Фото товара', null=True, blank=True)
    limited = models.BooleanField(verbose_name='Ограниченная серия', default=False)
    hit = models.BooleanField(verbose_name='Хит', default=False)
    description = RichTextField(verbose_name='Описание', null=True, blank=True)

    def __unicode__(self):
        return self.title


class ProductImage(AbstractModel):
    class Meta:
        verbose_name_plural = 'Изображения товара'
        verbose_name = 'Изображение товара'

    image = models.ImageField(verbose_name='Изображение', null=False)
    product = models.ForeignKey(verbose_name='Товар', to=Product, on_delete=models.CASCADE, null=False, related_name='images')

    def __unicode__(self):
        return str(self.pk)


class Order(AbstractModel):
    class Meta:
        verbose_name_plural = 'Заказы'
        verbose_name = 'Заказ'

    name = models.CharField(verbose_name='Имя', max_length=255, unique=False, null=False)
    phone = models.CharField(verbose_name='Телефон', max_length=255, unique=False, null=False)
    email = models.EmailField(verbose_name='Email', max_length=255, null=False)
    processed = models.BooleanField(verbose_name='Обработан?', default=False)

    def __unicode__(self):
        return self.name


class OrderItem(AbstractModel):
    class Meta:
        verbose_name_plural = 'Элементы заказа'
        verbose_name = 'Элемент заказа'

    product = models.ForeignKey(verbose_name='Товар', to=Product, on_delete=models.CASCADE, null=False)
    quantity = models.IntegerField(verbose_name='Количество', null=False)
    order = models.ForeignKey(verbose_name='Заказ', to=Order, on_delete=models.CASCADE, null=False, related_name='items')

    def __unicode__(self):
        return str(self.pk)


class About(AbstractModel):
    class Meta:
        verbose_name_plural = 'О нас'
        verbose_name = 'О нас'

    company_name = models.CharField(verbose_name='Название компании', max_length=255, unique=False, null=False, default='',
                                    blank=True)
    text = RichTextField(verbose_name='Текст', null=False, default='', blank=True)

    def __unicode__(self):
        return str(self.pk)


class Condition(OrderedModel):
    class Meta(OrderedModel.Meta):
        verbose_name_plural = 'Условия'
        verbose_name = 'Условие'

    title = models.CharField(verbose_name='Заголовок', max_length=255, unique=False, null=False)
    short_description = models.CharField(verbose_name='Краткое описание', max_length=1000, unique=False, null=False)
    description = RichTextField(verbose_name='Описание', null=True, blank=True)
    icon = models.CharField(verbose_name='Иконка', default='1', choices=ICON_CHOICES, max_length=5)

    def __unicode__(self):
        return self.title


class PriceListRequest(AbstractModel):
    class Meta:
        verbose_name_plural = 'Заявки для получения прайс-листа'
        verbose_name = 'Заявка для получения прайс-листа'

    email = models.EmailField(verbose_name='EMail', max_length=255, null=False)
    processed = models.BooleanField(verbose_name='Обработан?', default=False)

    def __unicode__(self):
        return self.email


class Requisites(AbstractModel):
    class Meta:
        verbose_name_plural = 'Оплата'
        verbose_name = 'Оплата'

    russia = RichTextField(verbose_name='В России', null=False)
    manufacturer = RichTextField(verbose_name='Производитель', null=False)
    first_text = RichTextField(verbose_name='Первый текст', null=True)
    warning_text = RichTextField(verbose_name='Текст-предупреждение', null=True)
    second_text = RichTextField(verbose_name='Второй текст', null=True)
    kg_retail_text = RichTextField(verbose_name='Контакты розн. магазинов (Кыргызстан)', default='', blank=True)
    ru_retail_text = RichTextField(verbose_name='Контакты розн. магазинов (Россия)', default='', blank=True)

    def __unicode__(self):
        return str(self.pk)


class Contacts(AbstractModel):
    class Meta:
        verbose_name_plural = 'Контакты'
        verbose_name = 'Контакты'

    info = RichTextField(verbose_name='Информация', null=False)
    phone = CharField(verbose_name='Номер телефона', max_length=255, null=True, blank=True)
    whatsapp = CharField(verbose_name='Whatsapp', max_length=255, null=True, blank=True)
    jivosite_code = TextField(verbose_name='Код Jivosite', null=True, blank=True)

    def __unicode__(self):
        return str(self.pk)


class Social(AbstractModel):
    class Meta:
        verbose_name_plural = 'Соц. сети'
        verbose_name = 'Соц. сети'

    disqus = models.CharField(verbose_name='Diesel', max_length=255, unique=False, null=True)
    vk = models.CharField(verbose_name='ВКонтакте', max_length=255, unique=False, null=True)
    instagram = models.CharField(verbose_name='Instagram', max_length=255, unique=False, null=True)
    fb = models.CharField(verbose_name='Facebook', max_length=255, unique=False, null=True)

    def __unicode__(self):
        return str(self.pk)


class Callback(AbstractModel):
    class Meta:
        verbose_name_plural = 'Обратный звонок'
        verbose_name = 'Обратный звонок'

    name = models.CharField(verbose_name='Имя', max_length=255, unique=False, null=False)
    phone = models.CharField(verbose_name='Контактный телефон', max_length=255, unique=False, null=False)
    email = models.EmailField(verbose_name='EMail', max_length=255, null=False)
    processed = models.BooleanField(verbose_name='Обработан?', default=False)

    def __unicode__(self):
        return self.name


class AboutCounter(AbstractModel):
    class Meta:
        verbose_name_plural = 'Счетчики'
        verbose_name = 'Счетчик'

    number = models.IntegerField(verbose_name='Число', null=False)
    text = models.CharField(verbose_name='Подпись', max_length=255, unique=False, null=False)
    about = models.ForeignKey(verbose_name='О нас', to=About, on_delete=models.CASCADE, null=False,
                              related_name='counters')

    def __unicode__(self):
        return '%s - %s' % (self.number, self.text)


class ConditionsPage(AbstractModel):
    class Meta:
        verbose_name_plural = 'Страница условий'
        verbose_name = 'Страница условий'

    warning_text = RichTextField(verbose_name='Текст-предупреждение', null=False)

    def __unicode__(self):
        return self.warning_text


class ConditionsPageBuyStep(AbstractModel):
    class Meta:
        verbose_name_plural = 'Как сделать покупку'
        verbose_name = 'Как сделать покупку'

    number = models.IntegerField(verbose_name='Номер', null=False)
    text = models.TextField(verbose_name='Подпись', null=False)
    conditions_page = models.ForeignKey(verbose_name='Страница условий', to=ConditionsPage, on_delete=models.CASCADE, null=False,
                                        related_name='buy_steps')

    def __unicode__(self):
        return '%s - %s' % (self.number, self.text)


class Delivery(AbstractModel):
    class Meta:
        verbose_name_plural = 'Доставка'
        verbose_name = 'Доставка'

    text = RichTextField(verbose_name='Текст', null=False)

    def __unicode__(self):
        return str(self.pk)


@receiver(post_save, sender=Product)
@receiver(post_save, sender=Condition)
def on_model_save(sender, instance, created, **kwargs):
    if created:
        instance.top()
        instance.save()


class RetailRequest(AbstractModel):
    class Meta:
        verbose_name_plural = 'Заявки оптовиков'
        verbose_name = 'Заявка оптовиков'

    document = models.FileField(verbose_name='document', null=False)

    def __unicode__(self):
        return str(self.pk)


class RetailShopCategory(AbstractModel):

    class Meta:
        verbose_name_plural = 'Виды деятельности'
        verbose_name = 'Вид деятельности'

    title = models.CharField(verbose_name='Название', max_length=255, unique=False, null=False)

    def __unicode__(self):
        return self.title


class RetailShopRequest(AbstractModel):

    class Meta:
        verbose_name_plural = 'Заявки для получения прайс-листа'
        verbose_name = 'Заявка для получения прайс-листа'

    category = models.ForeignKey(verbose_name='Категория', to=Category, on_delete=models.CASCADE, null=False)
    shop_category = models.ForeignKey(verbose_name='Вид деятельности', to=RetailShopCategory, on_delete=models.CASCADE, null=False)
    title = models.CharField(verbose_name='Название компании', max_length=255, unique=False, null=False)
    full_name = models.CharField(verbose_name='Фамилия и иимя', max_length=255, unique=False, null=False)
    phone = models.CharField(verbose_name='Контактный номер', max_length=255, unique=False, null=False)
    email = models.EmailField(verbose_name='EMail', max_length=255, null=False)
    country = models.CharField(verbose_name='Страна, город', max_length=255, unique=False, null=False)
    comment = models.TextField(verbose_name='Комментарий', null=True, blank=True)
    processed = models.BooleanField(verbose_name='Обработан?', default=False)

    def __unicode__(self):
        return self.title
