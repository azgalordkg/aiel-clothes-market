from django.forms import ModelForm

from main.models import Order, PriceListRequest, Category, Callback, RetailRequest, Product, RetailShopRequest


class OrderForm(ModelForm):

    class Meta:
        model = Order
        exclude = ()


class PriceListRequestForm(ModelForm):

    class Meta:
        model = PriceListRequest
        exclude = ()


class CategoryForm(ModelForm):

    class Meta:
        model = Category
        exclude = ('parent', )


class CallbackForm(ModelForm):

    class Meta:
        model = Callback
        exclude = ()


class RetailRequestForm(ModelForm):

    class Meta:
        model = RetailRequest
        exclude = ()


class ProductAdminForm(ModelForm):

    class Meta:
        model = Product
        exclude = ('price', 'price_linear', )


class RetailShopRequestForm(ModelForm):

    class Meta:
        model = RetailShopRequest
        exclude = ()