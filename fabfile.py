from fabric.context_managers import cd, prefix
from fabric.operations import sudo, run
from fabric.state import env

PROJECT_ROOT = '/home/sunrisedev/aiel'
VENV_DIR = '/home/sunrisedev/envs/aiel'


def update():
    env.host_string = '81.16.141.250'
    env.user = 'root'
    env.password = '79w86j2l9Ggh'
    with cd(PROJECT_ROOT):
        sudo('git stash')
        sudo('git pull origin master')
        with prefix('source ' + VENV_DIR + '/bin/activate'):
            run('pip install -r requirements/prod.txt')
            run('./manage.py collectstatic --noinput')
            run('./manage.py migrate')
            sudo('kill -1 `cat /var/run/uwsgi/app/aiel/pid`')