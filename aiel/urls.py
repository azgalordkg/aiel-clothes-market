"""djps URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from aiel import settings
from main.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'^jet/', include('jet.urls', 'jet')),
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'^about/$', AboutUsView.as_view(), name='about'),
    url(r'^conditions/$', ConditionsView.as_view(), name='conditions'),
    url(r'^payment/$', PaymentView.as_view(), name='payment'),
    url(r'^delivery/$', DeliveryView.as_view(), name='delivery'),
    url(r'^cart/$', CartView.as_view(), name='cart'),
    url(r'^catalogue/(?P<category_id>\d+)/$', CatalogueView.as_view(), name='catalogue'),
    url(r'^product/(?P<pk>\d+)/$', ProductDetailView.as_view(), name='product_detail'),
    url(r'^conditions/(?P<pk>\d+)/$', ConditionsDetailView.as_view(), name='conditions_detail'),
    url(r'^contacts/$', ContactsView.as_view(), name='contacts'),

    url(r'^cart/add/$', AddToCartView.as_view(), name='add_to_cart'),
    url(r'^cart/remove/$', RemoveFromCartView.as_view(), name='remove_from_cart'),
    url(r'^order/create/$', OrderCreateView.as_view(), name='order_create'),
    url(r'^plr/create/$', PriceListRequestCreateView.as_view(), name='plr_create'),
    url(r'^callback/create/$', CallbackCreateView.as_view(), name='callback_create'),
    url(r'^rr/create/$', RetailRequestCreateView.as_view(), name='rr_create'),
    url(r'^rsr/create/$', RetailShopRequestCreateView.as_view(), name='rsr_create'),
    url(r'^retail/$', RetailView.as_view(), name='retail'),
    url(r'^price-form/$', PriceFormView.as_view(), name='price-form'),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

handler404 = page_not_found