$(document).ready(function () {

    var downloadPrice = $('.download-price');
    var typingEmail = $('.type-email-container');

    $('.left').hover(
        function () {
            $('.right-icon').addClass('hover')
        },
        function () {
            $('.right-icon').removeClass('hover')
        }
    );

    $('.right').hover(
        function () {
            $('.left-icon').addClass('hover')
        },
        function () {
            $('.left-icon').removeClass('hover')
        }
    );

    $('.category > input').on('change', function () {
        var searchId = 'sub' + $(this).attr('data-id');

        $('input[data-id="%s"]'.replace('%s', searchId)).prop('checked', $(this).is(':checked'));
    });

    $('.filterOrderBy').on('click', function () {
        $('#id_order_by').attr('value', $(this).attr('data-key'));
        $('#filterForm').submit();
    });

    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        swipeToSlide: false,
        swipe: false,
        asNavFor: '.slider-nav'
    });

    $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        centerMode: true,
        vertical: true,
        focusOnSelect: true
    });

    $(".spincrement").spincrement({
        from: 0,
        duration: 3000
    });

    var showUIKitNotification = function (msg, status) {
        UIkit.notification({
            message: msg,
            status: status,
            pos: 'top-right',
            timeout: 5000
        });
    };

    var addToCart = function (productId, quantity, replaceQuantity, url) {
        var data = {
            product_id: productId,
            quantity: quantity,
            replace_quantity: replaceQuantity
        };

        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            dataType: 'json',
            success: function (data) {
                if (data.success) {
                    if (data.message) {
                        showUIKitNotification(data.message, 'success');
                    }
                    $('#basket').text(data.items_count);
                    $('#basketSum').text(data.total_sum);

                    var priceSpan = $('span.price-' + productId);
                    var header = $('h4.price-header-' + productId);

                    $(priceSpan).text(parseFloat($(priceSpan).attr('data-price')) * quantity);
                    $(header).text(parseInt(quantity) === 1 ? 'Цена за линейку' : 'Цена за линейки');
                }
            },
            error: function () {

            }
        });
    };

    downloadPrice.on('click', function () {
        downloadPrice.css({
            'display': 'none'
        });
        typingEmail.css({
            'display': 'block'
        })
    });

    $('a.addToCartBtn').on('click', function () {
        addToCart($(this).attr('data-product-id'), $(this).attr('data-quantity'),
            $(this).attr('data-replace-quantity'), $(this).attr('data-url'));
    });

    $('input.cartItemQuantity').on('change', function () {
        addToCart($(this).attr('data-product-id'), $(this).attr('value'),
            $(this).attr('data-replace-quantity'), $(this).attr('data-url'));
    });

    $('input.cartItemQuantity').on('keyup', function () {
        var value = $(this).val();

        if (value === '' || value === '0' || value.includes('-')) {
            var _val = value;

            value = '1';

            if (_val === '0' || _val.includes('-')) {
                $(this).val(value);
            }
        }

        $(this).attr('value', value);
        $(this).trigger('change');
    });

    $('.minus, .plus').on('click', function () {
        var input = $('input', $(this).parent());
        var value = parseInt($(input).attr('value'));

        if ($(this).hasClass('plus')) {
            ++value;
        } else {
            --value;

            if (value <= 0) {
                value = 1;
            }
        }

        $(input).attr('value', value);
        $(input).val(value);
        $(input).trigger('change');
    });

    $('.cartItemDeleteBtn').on('click', function () {

        if (confirm('Вы уверены?')) {
            var productId = $(this).attr('data-product-id');
            var data = {
                product_id: productId
            };

            $.ajax({
                type: 'POST',
                url: $(this).attr('data-url'),
                data: data,
                dataType: 'json',
                success: function (data) {
                    if (data.success) {
                        showUIKitNotification(data.message, 'success');
                        $('#basket').text(data.items_count);
                        $('#row-' + productId).remove();
                        $('#basketSum').text(data.total_sum);
                    }
                },
                error: function () {

                }
            });
        }
    });

    $('form.orderForm, form.plrForm, form.callbackForm, form.rrForm, form.rsrForm').on('submit', function (e) {
        e.preventDefault();

        var isOrderForm = $(this).hasClass('orderForm');
        var form = $(this);

        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: new FormData(this),
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function (data) {
                showUIKitNotification(data.message, data.success ? 'success' : 'danger');

                if (isOrderForm) {
                    $('#basket').text(data.items_count);
                    $('#basketSum').text(data.total_sum);
                    $('.basket-item').remove();
                }

                form[0].reset();
            },
            error: function () {

            }
        });
    });

    $('.submitPlrFormBtn').on('click', function (e) {
        e.preventDefault();

        $(this).parent().submit();
    });

    $(document).on('contextmenu', 'img', function (e) {
        e.preventDefault();
    });

    // $('span').attr('style', '');
});